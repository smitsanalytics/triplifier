FROM openjdk:8

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    curl \
    unzip \
    dos2unix

RUN python3 -m pip install rdflib requests

# install ontop libraries
COPY javaTool/target/triplifier-1.0-SNAPSHOT-jar-with-dependencies.jar triplifier.jar

# install python scripts
RUN mkdir /pyScripts
COPY pyScripts/uploadData.py /pyScripts/uploadData.py

# install run script
COPY run.sh /run.sh
RUN dos2unix run.sh

# set the run script
CMD ["bash", "run.sh"]